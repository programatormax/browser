unit ConfigFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.IniFiles, Vcl.StdCtrls, Vcl.CheckLst;

type
  TConfigForm = class(TForm)
    CheckListBox1: TCheckListBox;
    procedure CheckListBox1ClickCheck(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ShowConfig;
  end;

var
  ConfigForm: TConfigForm;

implementation

{$R *.dfm}


procedure TConfigForm.ShowConfig;
var Ini: TIniFile;
    n: Integer;
begin
  n:=0;
  CheckListBox1.Items.Clear;
  Ini:=TIniFile.Create('.\Browser.ini');
  while Ini.ValueExists('List',IntToStr(n))=True do
  begin
    CheckListBox1.Items.Add(Ini.ReadString('List',IntToStr(n),'(ERROR)'));
    CheckListBox1.Checked[n]:=Ini.ReadBool('Checked',IntToStr(n),False);
    n:=n+1;
  end;
  Ini.Destroy;
  Ini:=nil;
  ShowModal;
end;


procedure TConfigForm.CheckListBox1ClickCheck(Sender: TObject);
var Ini: TIniFile;
    n: Integer;
begin
  Ini:=TIniFile.Create('.\Browser.ini');
  for n:=0 to CheckListBox1.Items.Count-1 do
  begin
    Ini.WriteBool('Checked',IntToStr(n),CheckListBox1.Checked[n]);
  end;
  Ini.Destroy;
  Ini:=nil;
end;

end.

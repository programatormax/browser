unit BrowserUnit;

{$I cef.inc}

interface

uses
  {$IFDEF DELPHI16_UP}
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  {$ELSE}
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  {$ENDIF}
  uCEFChromium, uCEFWindowParent, uCEFChromiumWindow, uCEFTypes, uCEFInterfaces,
  uCEFWinControl, uCEFSentinel, Vcl.ComCtrls, BrowserDB, Vcl.Buttons, ConfigFormUnit,
  System.ImageList, Vcl.ImgList, System.IniFiles, Vcl.Imaging.pngimage,
  Vcl.Imaging.jpeg, uCEFChromiumCore;

type

  TForm1 = class(TForm)
    Timer1: TTimer;
    FindPanel: TPanel;
    PageControl1: TPageControl;
    ComboBox1: TComboBox;
    StatusBar1: TStatusBar;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ImageList1: TImageList;
    TabSheet0: TTabSheet;
    Navigator1: TPanel;
    Splitter1: TSplitter;
    Search1: TPanel;
    SearchImage1: TImage;
    Search2: TPanel;
    SearchImage2: TImage;
    Search3: TPanel;
    SearchImage3: TImage;
    Logo: TPanel;
    LogoImage: TImage;
    ChromiumWindow1: TChromiumWindow;
    ChromiumWindow2: TChromiumWindow;
    ChromiumWindow3: TChromiumWindow;
    ChromiumWindow4: TChromiumWindow;
    SearchLabel1: TLabel;
    SearchLabel2: TLabel;
    SearchLabel3: TLabel;
    Search4: TPanel;
    SearchImage4: TImage;
    SearchLabel4: TLabel;
    Timer2: TTimer;
    TabSheet1: TTabSheet;
    ListHistory1: TListView;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure Timer1Timer(Sender: TObject);
    procedure OnTextEvent(Sender: TObject; const browser: ICefBrowser; const value: ustring);

    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ChromiumWindow1Close(Sender: TObject);
    procedure ChromiumWindow1BeforeClose(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SearchImage2Click(Sender: TObject);
    procedure SearchImage1Click(Sender: TObject);
    procedure SearchImage3Click(Sender: TObject);
    procedure SearchImage4Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);

  private
    FDB: TBrowserDB;
    // You have to handle this two messages to call NotifyMoveOrResizeStarted or some page elements will be misaligned.
    procedure WMMove(var aMessage : TWMMove); message WM_MOVE;
    procedure WMMoving(var aMessage : TMessage); message WM_MOVING;
    // You also have to handle these two messages to set GlobalCEFApp.OsmodalLoop
    procedure WMEnterMenuLoop(var aMessage: TMessage); message WM_ENTERMENULOOP;
    procedure WMExitMenuLoop(var aMessage: TMessage); message WM_EXITMENULOOP;

  protected
    // Variables to control when can we destroy the form safely
    FCanClose : boolean;  // Set to True in TChromium.OnBeforeClose
    FClosing  : boolean;  // Set to True in the CloseQuery event.

    procedure Chromium_OnBeforePopup(Sender: TObject; const browser: ICefBrowser; const frame: ICefFrame; const targetUrl, targetFrameName: ustring; targetDisposition: TCefWindowOpenDisposition; userGesture: Boolean; const popupFeatures: TCefPopupFeatures; var windowInfo: TCefWindowInfo; var client: ICefClient; var settings: TCefBrowserSettings; var extra_info: ICefDictionaryValue; var noJavascriptAccess: Boolean; var Result: Boolean);

  public
    property DB: TBrowserDB read FDB;
    { Public declarations }
  end;

var
  Form1: TForm1;
  FMenuTimeOut: Integer;

implementation

{$R *.dfm}

uses
  uCEFApplication;

// This is a demo with the simplest web browser you can build using CEF4Delphi and
// it doesn't show any sign of progress like other web browsers do.

// Remember that it may take a few seconds to load if Windows update, your antivirus or
// any other windows service is using your hard drive.

// Depending on your internet connection it may take longer than expected.

// Please check that your firewall or antivirus are not blocking this application
// or the domain "google.com". If you don't live in the US, you'll be redirected to
// another domain which will take a little time too.

// Destruction steps
// =================
// 1. The FormCloseQuery event sets CanClose to False and calls TChromiumWindow.CloseBrowser, which triggers the TChromiumWindow.OnClose event.
// 2. The TChromiumWindow.OnClose event calls TChromiumWindow.DestroyChildWindow which triggers the TChromiumWindow.OnBeforeClose event.
// 3. TChromiumWindow.OnBeforeClose sets FCanClose := True and sends WM_CLOSE to the form.


// This function converts a string into a RFC 1630 compliant URL
function URLEncode(Value : String) : String;
const
  ValidURLChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$-_@.&+-!*"''(),;/#?:';
Var I : Integer;
Begin
   Result := '';
   For I := 1 To Length(Value) Do
      Begin
         If Pos(UpperCase(Value[I]), ValidURLChars) > 0 Then
            Result := Result + Value[I]
         Else
            Begin
               If Value[I] = ' ' Then
                  Result := Result + '+'
               Else
                  Begin
                     Result := Result + '%';
                     Result := Result + IntToHex(Byte(Value[I]), 2);
                  End;
            End;
      End;
End;

procedure GlobalReseach;
var Ini: TIniFile;
      n: Integer;
      C: String;
      S: String;
begin
  Form1.PageControl1.ActivePageIndex:=0;
  FMenuTimeOut:=0;
  Form1.Timer2.Enabled:=True;

  Form1.Splitter1.Visible:=False;
  Form1.Navigator1.Visible:=False;
  Form1.Search1.Visible:=False;
  Form1.Search2.Visible:=False;
  Form1.Search3.Visible:=False;
  Form1.Search4.Visible:=False;

  Form1.ChromiumWindow1.Visible:=False;
  Form1.ChromiumWindow2.Visible:=False;
  Form1.ChromiumWindow3.Visible:=False;
  Form1.ChromiumWindow4.Visible:=False;
  Form1.Logo.Visible:=False;

  Form1.TabSheet0.TabVisible:=True;
  Form1.TabSheet0.ImageIndex:=-1;
  Form1.TabSheet0.Caption:='Search';


    Form1.ComboBox1.Items.Add(Trim(Form1.ComboBox1.Text));
    n:=0;
    Ini:=TIniFile.Create('.\Browser.ini');
    if (Pos('://',Form1.ComboBox1.Text)>0) or
       (Pos('www.',Form1.ComboBox1.Text)>0) or
       (Pos('.gov',Form1.ComboBox1.Text)>0) or
       (Pos('.com',Form1.ComboBox1.Text)>0) or
       (Pos('.edu',Form1.ComboBox1.Text)>0) or
       (Pos('.ru',Form1.ComboBox1.Text)>0) then
    begin
      Form1.DB.History(TYPE_LINK,Form1.ComboBox1.Text);
      Form1.ChromiumWindow1.LoadURL(Form1.ComboBox1.Text);
      Form1.ChromiumWindow1.Visible:=True;
    end
    else
    begin
      Form1.Navigator1.Width:=256;
      Form1.Navigator1.Visible:=True;
      Form1.Splitter1.Visible:=True;
      while Ini.ValueExists('List',IntToStr(n))=True do
      begin
        if Ini.ReadBool('Checked',IntToStr(n),False)=True then
        begin
          C:=Ini.ReadString('List',IntToStr(n),'Search')+' - '+Trim(Form1.ComboBox1.Text);
          S:=StringReplace(Ini.ReadString('Search',IntToStr(n),''),'%Search%',URLEncode(Trim(Form1.ComboBox1.Text)),[rfReplaceAll, rfIgnoreCase]);
          case n of
            0:
            begin
              Form1.Search1.Visible:=True;
              Form1.SearchLabel1.Caption:=C;
              Form1.SearchImage1.Picture.LoadFromFile('.\SearchImage1.jpg');
              Form1.ChromiumWindow1.LoadURL(S);
            end;
            1:
            begin
              Form1.Search2.Visible:=True;
              Form1.SearchLabel2.Caption:=C;
              Form1.SearchImage2.Picture.LoadFromFile('.\SearchImage2.jpg');
              Form1.ChromiumWindow2.LoadURL(S);
            end;
            2:
            begin
              Form1.Search3.Visible:=True;
              Form1.SearchLabel3.Caption:=C;
              Form1.SearchImage3.Picture.LoadFromFile('.\SearchImage3.jpg');
              Form1.ChromiumWindow3.LoadURL(S);
            end;
            3:
            begin
              Form1.Search4.Visible:=True;
              Form1.SearchLabel4.Caption:=C;
              Form1.SearchImage4.Picture.LoadFromFile('.\SearchImage3.jpg');
              Form1.ChromiumWindow4.LoadURL(S);
            end;
          end;
        end;
        n:=n+1;
      end;
      Form1.DB.History(TYPE_INFORMATION,Form1.ComboBox1.Text);
    end;
    Ini.Destroy;
    Ini:=nil;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned(FDB) then FreeAndNil(FDB);
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := FCanClose;

  if not(FClosing) then
    begin
      FClosing := True;
      Visible  := False;
      ChromiumWindow1.CloseBrowser(True);
      ChromiumWindow2.CloseBrowser(True);
      ChromiumWindow3.CloseBrowser(True);
      ChromiumWindow4.CloseBrowser(True);
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FCanClose := False;
  FClosing  := False;
  FDB:=TBrowserDB.Create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  if Assigned(FDB) then FreeAndNil(FDB);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  FDB.GetSearchHistory(ComboBox1.Items);

  // For simplicity, this demo blocks all popup windows and new tabs
  ChromiumWindow1.ChromiumBrowser.OnBeforePopup := Chromium_OnBeforePopup;
  ChromiumWindow2.ChromiumBrowser.OnBeforePopup := Chromium_OnBeforePopup;
  ChromiumWindow3.ChromiumBrowser.OnBeforePopup := Chromium_OnBeforePopup;
  ChromiumWindow4.ChromiumBrowser.OnBeforePopup := Chromium_OnBeforePopup;

  ChromiumWindow1.ChromiumBrowser.OnStatusMessage:=OnTextEvent;
  ChromiumWindow2.ChromiumBrowser.OnStatusMessage:=OnTextEvent;
  ChromiumWindow3.ChromiumBrowser.OnStatusMessage:=OnTextEvent;
  ChromiumWindow4.ChromiumBrowser.OnStatusMessage:=OnTextEvent;

  // You *MUST* call CreateBrowser to create and initialize the browser.
  // This will trigger the AfterCreated event when the browser is fully
  // initialized and ready to receive commands.

  // GlobalCEFApp.GlobalContextInitialized has to be TRUE before creating any browser
  // If it's not initialized yet, we use a simple timer to create the browser later.
  if not(ChromiumWindow1.CreateBrowser) then Timer1.Enabled := True;
  if not(ChromiumWindow2.CreateBrowser) then Timer1.Enabled := True;
  if not(ChromiumWindow3.CreateBrowser) then Timer1.Enabled := True;
  if not(ChromiumWindow4.CreateBrowser) then Timer1.Enabled := True;
end;

procedure TForm1.SearchImage1Click(Sender: TObject);
begin
  Form1.ChromiumWindow4.Visible:=False;
  Form1.ChromiumWindow3.Visible:=False;
  Form1.ChromiumWindow2.Visible:=False;
  Form1.ChromiumWindow1.Visible:=True;
  FMenuTimeOut:=0;
  Timer2.Enabled:=True;
end;

procedure TForm1.SearchImage2Click(Sender: TObject);
begin
  Form1.ChromiumWindow4.Visible:=False;
  Form1.ChromiumWindow3.Visible:=False;
  Form1.ChromiumWindow1.Visible:=False;
  Form1.ChromiumWindow2.Visible:=True;
  FMenuTimeOut:=0;
  Timer2.Enabled:=True;
end;

procedure TForm1.SearchImage3Click(Sender: TObject);
begin
  Form1.ChromiumWindow4.Visible:=False;
  Form1.ChromiumWindow2.Visible:=False;
  Form1.ChromiumWindow1.Visible:=False;
  Form1.ChromiumWindow3.Visible:=True;
  FMenuTimeOut:=0;
  Timer2.Enabled:=True;
end;

procedure TForm1.SearchImage4Click(Sender: TObject);
begin
  Form1.ChromiumWindow3.Visible:=False;
  Form1.ChromiumWindow2.Visible:=False;
  Form1.ChromiumWindow1.Visible:=False;
  Form1.ChromiumWindow4.Visible:=True;
  FMenuTimeOut:=0;
  Timer2.Enabled:=True;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  if TabSheet1.TabVisible=True then
  begin
    TabSheet1.TabVisible:=False;
  end
  else
  begin
    DB.GetListHistory(ListHistory1);
    PageControl1.ActivePageIndex:=1;
    TabSheet1.TabVisible:=True;
  end;
end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
begin
  ConfigForm.ShowConfig;
end;

procedure TForm1.SpeedButton3Click(Sender: TObject);
begin
  if ChromiumWindow1.Visible=True then ChromiumWindow1.ChromiumBrowser.GoBack;
  if ChromiumWindow2.Visible=True then ChromiumWindow2.ChromiumBrowser.GoBack;
  if ChromiumWindow3.Visible=True then ChromiumWindow3.ChromiumBrowser.GoBack;
  if ChromiumWindow4.Visible=True then ChromiumWindow4.ChromiumBrowser.GoBack;
end;

procedure TForm1.SpeedButton4Click(Sender: TObject);
begin
  if ChromiumWindow1.Visible=True then ChromiumWindow1.ChromiumBrowser.GoForward;
  if ChromiumWindow2.Visible=True then ChromiumWindow2.ChromiumBrowser.GoForward;
  if ChromiumWindow3.Visible=True then ChromiumWindow3.ChromiumBrowser.GoForward;
  if ChromiumWindow4.Visible=True then ChromiumWindow4.ChromiumBrowser.GoForward;
end;

procedure TForm1.Button1Click(Sender: TObject);
var Bitmap: TBitmap;
begin
  if ChromiumWindow1.ChromiumBrowser<>nil then
  begin
    Bitmap:=nil;
    Form1.Caption:='sdfsd';
    ChromiumWindow1.ChromiumBrowser.TakeSnapshot(Bitmap);
    Bitmap.SaveToFile('C:\ProgramData\qqq.bmp');

  end
  else
  begin
    Form1.Caption:='Fault';
  end;
end;

procedure TForm1.ChromiumWindow1BeforeClose(Sender: TObject);
begin
  FCanClose := True;
  PostMessage(Handle, WM_CLOSE, 0, 0);
end;

procedure TForm1.ChromiumWindow1Close(Sender: TObject);
begin
  // DestroyChildWindow will destroy the child window created by CEF at the top of the Z order.
  if not(ChromiumWindow1.DestroyChildWindow) then
    begin
      FCanClose := True;
      PostMessage(Handle, WM_CLOSE, 0, 0);
    end;

    if not(ChromiumWindow2.DestroyChildWindow) then
    begin
      FCanClose := True;
      PostMessage(Handle, WM_CLOSE, 0, 0);
    end;

    if not(ChromiumWindow3.DestroyChildWindow) then
    begin
      FCanClose := True;
      PostMessage(Handle, WM_CLOSE, 0, 0);
    end;

    if not(ChromiumWindow4.DestroyChildWindow) then
    begin
      FCanClose := True;
      PostMessage(Handle, WM_CLOSE, 0, 0);
    end;

end;

procedure TForm1.Chromium_OnBeforePopup(      Sender             : TObject;
                                        const browser            : ICefBrowser;
                                        const frame              : ICefFrame;
                                        const targetUrl          : ustring;
                                        const targetFrameName    : ustring;
                                              targetDisposition  : TCefWindowOpenDisposition;
                                              userGesture        : Boolean;
                                        const popupFeatures      : TCefPopupFeatures;
                                        var   windowInfo         : TCefWindowInfo;
                                        var   client             : ICefClient;
                                        var   settings           : TCefBrowserSettings;
                                        var   extra_info         : ICefDictionaryValue;
                                        var   noJavascriptAccess : Boolean;
                                        var   Result             : Boolean);
begin
  // For simplicity, this demo blocks all popup windows and new tabs
  Result := (targetDisposition in [WOD_NEW_FOREGROUND_TAB, WOD_NEW_BACKGROUND_TAB, WOD_NEW_POPUP, WOD_NEW_WINDOW]);
end;

procedure TForm1.ComboBox1KeyPress(Sender: TObject; var Key: Char);
var Ini: TIniFile;
      n: Integer;
      C: String;
      S: String;
begin
  if Key=#13 then GlobalReseach;
end;

procedure TForm1.ComboBox1Select(Sender: TObject);
begin
  //GlobalReseach;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if not(ChromiumWindow1.CreateBrowser) and not(ChromiumWindow1.Initialized) then Timer1.Enabled := True;
  if not(ChromiumWindow2.CreateBrowser) and not(ChromiumWindow2.Initialized) then Timer1.Enabled := True;
  if not(ChromiumWindow3.CreateBrowser) and not(ChromiumWindow3.Initialized) then Timer1.Enabled := True;
  if not(ChromiumWindow4.CreateBrowser) and not(ChromiumWindow4.Initialized) then Timer1.Enabled := True;
end;

procedure TForm1.OnTextEvent(Sender: TObject; const browser: ICefBrowser; const value: ustring);
begin
  StatusBar1.SimpleText:=value;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
var Bit: TBitmap;
begin
  if FMenuTimeOut>10 then
  begin
    while Navigator1.Width>8 do
    begin
      Navigator1.Width:=Navigator1.Width-25;
      Sleep(1);
      Application.ProcessMessages;
    end;
    FMenuTimeOut:=0;
    Timer2.Enabled:=False;
    Navigator1.Width:=8;
  end;
  FMenuTimeOut:=FMenuTimeOut+1;
  if ChromiumWindow1.Visible=True then
  begin
    Bit:=nil;
    ChromiumWindow1.ChromiumBrowser.TakeSnapshot(Bit);
    Bit.SaveToFile('.\TempImage1.bmp');
    Bit.Destroy;
    Bit:=nil;
    SearchImage1.Picture.LoadFromFile('.\TempImage1.bmp');
  end;
  if ChromiumWindow2.Visible=True then
  begin
    Bit:=nil;
    ChromiumWindow2.ChromiumBrowser.TakeSnapshot(Bit);
    Bit.SaveToFile('.\TempImage2.bmp');
    Bit.Destroy;
    Bit:=nil;
    SearchImage2.Picture.LoadFromFile('.\TempImage2.bmp');
  end;
  if ChromiumWindow3.Visible=True then
  begin
    Bit:=nil;
    ChromiumWindow3.ChromiumBrowser.TakeSnapshot(Bit);
    Bit.SaveToFile('.\TempImage3.bmp');
    Bit.Destroy;
    Bit:=nil;
    SearchImage3.Picture.LoadFromFile('.\TempImage3.bmp');
  end;
  if ChromiumWindow4.Visible=True then
  begin
    Bit:=nil;
    ChromiumWindow4.ChromiumBrowser.TakeSnapshot(Bit);
    Bit.SaveToFile('.\TempImage4.bmp');
    Bit.Destroy;
    Bit:=nil;
    SearchImage4.Picture.LoadFromFile('.\TempImage4.bmp');
  end;
end;

procedure TForm1.WMMove(var aMessage : TWMMove);
begin
  inherited;
  if (ChromiumWindow1 <> nil) then ChromiumWindow1.NotifyMoveOrResizeStarted;
  if (ChromiumWindow2 <> nil) then ChromiumWindow2.NotifyMoveOrResizeStarted;
  if (ChromiumWindow3 <> nil) then ChromiumWindow3.NotifyMoveOrResizeStarted;
  if (ChromiumWindow4 <> nil) then ChromiumWindow4.NotifyMoveOrResizeStarted;
end;

procedure TForm1.WMMoving(var aMessage : TMessage);
begin
  inherited;
  if (ChromiumWindow1 <> nil) then ChromiumWindow1.NotifyMoveOrResizeStarted;
  if (ChromiumWindow2 <> nil) then ChromiumWindow2.NotifyMoveOrResizeStarted;
  if (ChromiumWindow3 <> nil) then ChromiumWindow3.NotifyMoveOrResizeStarted;
  if (ChromiumWindow4 <> nil) then ChromiumWindow4.NotifyMoveOrResizeStarted;
end;

procedure TForm1.WMEnterMenuLoop(var aMessage: TMessage);
begin
  inherited;
  if (aMessage.wParam = 0) and (GlobalCEFApp <> nil) then GlobalCEFApp.OsmodalLoop := True;
end;

procedure TForm1.WMExitMenuLoop(var aMessage: TMessage);
begin
  inherited;
  if (aMessage.wParam = 0) and (GlobalCEFApp <> nil) then GlobalCEFApp.OsmodalLoop := False;
end;

end.

unit BrowserDB;

interface

uses

  Windows,
  SysUtils,
  Classes,
  DateUtils,
  SqliteTable3,
  Vcl.ComCtrls,
  EntityProjectUnit;

const

  TABLE_NAME = 'browser';

  TYPE_INFORMATION = 0;
  TYPE_LINK        = 1;
  TYPE_START       = 3;
  TYPE_STOP        = 4;

type

  TBrowserDB = class
  private
    FSqlite: TSQLiteDatabase;
    function GetBrowserPath: AnsiString;
  public
    function History(Id: UInt64; Str: AnsiString): Boolean;
    function GetSearchHistory(Strings: TStrings): Boolean;
    function GetListHistory(List: TListView): Boolean;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

//Create class
constructor TBrowserDB.Create;
var Req: AnsiString;
begin
  FSqlite:=TSQLiteDatabase.Create(GetBrowserPath+'Browser.sqlite');
  FSqlite.ExecSQL('PRAGMA synchronous=0');
  FSqlite.ExecSQL('PRAGMA encoding = "UTF-8"');
  FSqlite.ExecSQL('PRAGMA cache_size=3000');
  FSqlite.ExecSQL('PRAGMA temp_store=MEMORY');
  if FSqlite.TableExists('browser')= False then
  begin
    Req:='create table '+TABLE_NAME+' (';
    Req:=Req+'datetimes INTEGER,';
    Req:=Req+'types INTEGER,';
    Req:=Req+'informs TEXT)';
    FSqlite.ExecSQL(Req);
  end;
  Req:='Browser start. OS: '+Core.Name+';User:'+Core.Login+';Admin: '+BoolToStr(Core.Admin,True);
  History(TYPE_START,Req);
end;

//Destroy class
destructor TBrowserDB.Destroy;
begin
  History(TYPE_START,'Browser stop');
  FSqlite.Destroy;
  FSqlite:=nil;
  inherited Destroy;
end;

//Get current browser path
function TBrowserDB.GetBrowserPath: AnsiString;
begin
  Result:=ExtractFilePath(ParamStr(0));
end;

//Ass history action
function TBrowserDB.History(Id: UInt64; Str: AnsiString): Boolean;
var Sql: AnsiString;
begin
  Result:=False;
  if Str<>'' then
  begin
    Sql:='insert into '+TABLE_NAME+' (datetimes,types,informs) values (';
    Sql:=Sql+IntToStr(DateTimeToUnix(Now))+',';
    Sql:=Sql+IntToStr(Id)+',';
    Sql:=Sql+'"'+Trim(Str)+'");';
    try
      FSqlite.ExecSQL(Sql);
      Result:=True;
    except
      Result:=False;
    end;
  end;
end;

//Get research
function TBrowserDB.GetSearchHistory(Strings: TStrings): Boolean;
var Select: TSQLiteTable;
    n: Integer;
begin
  Result:=False;
  if Assigned(Strings) then
  begin
    Strings.Clear;
    Select:=FSqlite.GetTable('select * from '+TABLE_NAME+' where types=0 or types=1');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        for n:=0 to Select.Count-1 do
        begin
          Strings.Add(Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end;
  end;
end;

//List History
function TBrowserDB.GetListHistory(List: TListView): Boolean;
var Select: TSQLiteTable;
    n: Integer;
    Item: TListItem;
begin
  Result:=False;
  if Assigned(List) then
  begin
    List.Items.BeginUpdate;
    List.Items.Clear;
    Select:=FSqlite.GetTable('select * from '+TABLE_NAME+' order by datetimes desc');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        for n:=0 to Select.Count-1 do
        begin
          Item:=List.Items.Add;
          Item.Caption:=DateTimeToStr(UnixToDateTime(Select.FieldAsInteger(0)));
          case Select.FieldAsInteger(1) of
            TYPE_INFORMATION:
            begin
              Item.ImageIndex:=0;
              Item.SubItems.Add('Info');
            end;
            TYPE_LINK:
            begin
              Item.ImageIndex:=1;
              Item.SubItems.Add('Link');
            end;
            TYPE_START:
            begin
              Item.ImageIndex:=2;
              Item.SubItems.Add('Start');
            end;
            TYPE_STOP:
            begin
              Item.ImageIndex:=2;
              Item.SubItems.Add('Stop');
            end;
          end;
          Item.SubItems.Add(Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end;
    List.Items.EndUpdate;
  end;
end;

end.
